package com.theendcomplete.barcalculator2;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class GridFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    private static GridViewAdapter mAdapter;
    private static TextView tvSum;
    final String MY_LOG = "BarCalc Log";

    public GridFragment() {
        // Required empty public constructor
    }

    public static GridFragment newInstance() {
        GridFragment fragment = new GridFragment();
        return fragment;
    }


    public static void Update() {
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mAdapter = new GridViewAdapter(getActivity(), RestoreArray());
        View view = inflater.inflate(R.layout.fragment_grid, container, false);
        GridView gridView = (GridView) view.findViewById(R.id.gvMain);
        gridView.setAdapter(mAdapter);
        gridView.setOnItemClickListener(this);
        gridView.setOnItemLongClickListener(this);

        tvSum = (TextView) view.findViewById(R.id.tvSum);
        tvSum.setText(getString(R.string.sum) + Float.toString(((Main) getActivity()).CountArray()));
        Button btnAdd = (Button) view.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddDrink();
                Update();
            }
        });

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        MyDBHelper dbHelper = new MyDBHelper(getActivity());
        dbHelper.Save((Drink) parent.getItemAtPosition(position));
        ((Drink) parent.getItemAtPosition(position)).setCount(1 + ((Drink) parent.getItemAtPosition(position)).getCount());
        tvSum.setText(getString(R.string.sum) + Float.toString(((Main) getActivity()).CountArray()));
        Update();
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        ShowSettings(((Drink) parent.getItemAtPosition(position)), position);
        return true;
    }

    public void onPause() {
        super.onPause();
        Context mContext = getActivity().getApplicationContext();
        SharedPreferences mPrefs = mContext.getSharedPreferences("listDrink", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        Gson gson;
        gson = new Gson();
        String json = gson.toJson(((Main) getActivity()).listDrink);
        editor.putString("listDrink", json);
        editor.apply();//Было commit
    }

    public void onResume() {
        super.onResume();
        tvSum.setText(getString(R.string.sum) + Float.toString(((Main) getActivity()).CountArray()));
    }

    public ArrayList<Drink> RestoreArray() {
        Gson gson = new Gson();
        String json;
        Context mContext = getActivity().getApplicationContext();
        SharedPreferences mPrefs = mContext.getSharedPreferences("listDrink", Context.MODE_PRIVATE);

        json = mPrefs.getString("listDrink", "");
        Type type = new TypeToken<ArrayList<Drink>>() {
        }.getType();


        if (!json.isEmpty()) {
            ((Main) getActivity()).listDrink = gson.fromJson(json, type);
        }
        return ((Main) getActivity()).listDrink;
    }


    private void ShowSettings(Drink drink, int position) {

        android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
//        ft.commit();

        FragmentSettings settings = FragmentSettings.newInstance(drink, position);

        settings.show(getFragmentManager(), "fragmentDialog");


    }


    private void AddDrink() {
        Main main = (Main) getActivity();

        Drink drinkNew = new Drink(getString(R.string.newItem), 500f, 35f, 0);
        int newPosition = main.listDrink.size();
        ShowSettings(drinkNew, newPosition);
        tvSum.setText(getString(R.string.sum) + Float.toString(((Main) getActivity()).CountArray()));

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((Main) activity).onSectionAttached(1);
    }
}
