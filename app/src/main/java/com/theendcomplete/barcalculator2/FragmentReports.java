package com.theendcomplete.barcalculator2;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import com.astuetz.PagerSlidingTabStrip;


public class FragmentReports extends Fragment {
    private TabHost mTabHost;
    private ViewPager mViewPager;
    private YourAdapter mTabsAdapter;

    public FragmentReports() {
    }

    public static FragmentReports newInstance() {
        FragmentReports fragment = new FragmentReports();
        return fragment;
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reports, container, false);
//        AdView mAdView = (AdView) rootView.findViewById(R.id.adView);
//        YourAdapter tabadapter = new YourAdapter(getActivity());
//        AdRequest adRequest = new AdRequest.Builder().addTestDevice("80B4DF71BDC54022B6A483DA8AD6EC71").addTestDevice("TEST_EMULATOR").build();
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        mTabHost = new FragmentTabHost(getActivity());

        mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
        mTabsAdapter = new YourAdapter(getActivity().getSupportFragmentManager());
        mViewPager.setAdapter(mTabsAdapter);
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        tabs.setShouldExpand(true);
        tabs.setViewPager(mViewPager);
        return rootView;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((Main) activity).onSectionAttached(5);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTabHost = null;
    }
}
