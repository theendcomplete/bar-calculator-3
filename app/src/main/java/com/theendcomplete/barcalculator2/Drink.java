package com.theendcomplete.barcalculator2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nicko_000 on 21.01.2015.
 */
public class Drink implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Drink> CREATOR = new Parcelable.Creator<Drink>() {
        @Override
        public Drink createFromParcel(Parcel in) {
            return new Drink(in);
        }

        @Override
        public Drink[] newArray(int size) {
            return new Drink[size];
        }
    };
    private int _id;
    private String _productname;//
    private float _volume;//
    private float _proof;//
    private float _price;//
    private String _comment;//
    private int _stars; //
    private String _gps;//
    private int _type; //тип напитка, 1 - пиво, 2 - темное, 3 - крепкий, 4 - коктейль, 5 - еда, 6 - еда
    private String _time;//
    private float _count;//

    public Drink() {

    }

    public Drink(int id, String productname, float volume, float proof) {
        this._id = id;
        this._productname = productname;
        this._volume = volume;
        this._proof = proof;
    }


    public Drink(String productname, float volume, float proof, int type) {

        this._productname = productname;
        this._volume = volume;
        this._proof = proof;
        this._type = type;
    }


    //Конструктоp с типами


    public Drink(String productname, float volume, float proof) {

        this._productname = productname;
        this._volume = volume;
        this._proof = proof;
    }


    public Drink(String _productname, float _volume, float _proof, float _price, String _comment, int _type) {
        this._productname = _productname;
        this._volume = _volume;
        this._proof = _proof;
        this._price = _price;
        this._comment = _comment;
        this._type = _type;
    }

    private Drink(Parcel in) {
        _id = in.readInt();
        _productname = in.readString();
        _volume = in.readFloat();
        _proof = in.readFloat();
        _price = in.readFloat();
        _comment = in.readString();
        _stars = in.readInt();
        _gps = in.readString();
        _type = in.readInt();
        _time = in.readString();
    }

    public float getCount() {
        return _count;
    }

    public void setCount(float _count) {
        this._count = _count;
    }

    public int getID() {
        return this._id;
    }

    public void setID(int id) {
        this._id = id;
    }

    public String getProductName() {
        return this._productname;
    }

    public void setProductName(String productname) {
        this._productname = productname;
    }

    public float getVolume() {
        return this._volume;
    }

    public void setVolume(float volume) {
        this._volume = volume;
    }

    public float getProof() {
        return this._proof;
    }

    public void setProof(float proof) {
        this._proof = proof;
    }

    public float getPrice() {
        return _price;
    }

    public void setPrice(float price) {
        this._price = price;
    }

    public String getComment() {
        return _comment;
    }

    public void setComment(String comment) {
        this._comment = comment;
    }

    public int getStars() {
        return _stars;
    }

    public void setStars(int stars) {
        this._stars = stars;
    }

    public String getGps() {
        return _gps;
    }

    public void setGps(String gps) {
        this._gps = gps;
    }

    public int getType() {
        return _type;
    }

    public void setType(int type) {
        this._type = type;
    }

    public String getTime() {
        return _time;
    }

    public void setTime(String _time) {
        this._time = _time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_id);
        dest.writeString(_productname);
        dest.writeFloat(_volume);
        dest.writeFloat(_proof);
        dest.writeFloat(_price);
        dest.writeString(_comment);
        dest.writeInt(_stars);
        dest.writeString(_gps);
        dest.writeInt(_type);
        dest.writeString(_time);
    }
}