package com.theendcomplete.barcalculator2;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MyDBHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "barcalc.db";
    private static final String TABLE_NAME = "drinks";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE \"drinks\" (\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE," +
                    " \"_productname\" TEXT, \"_proof\" TEXT, \"_volume\" TEXT, \"_type\" TEXT," +
                    " \"_gps\" TEXT, \"_photo\" BLOB, \"_price\" TEXT, \"_comment\" TEXT," +
                    " \"_stars\" INTEGER, \"_time\"DATETIME DEFAULT CURRENT_TIMESTAMP)";


    public MyDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);

    }

    public void DeleteTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from drinks");
        db.execSQL("vacuum");
        db.close();
    }


    public int Count(String product) {
//        MyDBHelper dbHelper = new MyDBHelper(this);
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT  * FROM \"drinks\" WHERE _productname = '" + product + "' ;", null);
        int i = c.getCount();
        c.close();
        db.close();
//        Log.d("MyLog", "   " + i);
        return i;
    }

    public void Save(Drink drink) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("_productname", drink.getProductName());
        cv.put("_volume", drink.getVolume());
        cv.put("_proof", drink.getProof());
        cv.put("_type", drink.getType());
        cv.put("_gps", drink.getGps());
//        cv.put("_photo", drink.getPhoto());
        cv.put("_price", drink.getPrice());
        cv.put("_comment", drink.getComment());
        cv.put("_stars", drink.getStars());


        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        Date now = new Date();

        cv.put("_time", String.valueOf(sdf.format(now)));
//        cv.put("_time", Float.valueOf(String.valueOf(seconds)));

        db.insert("drinks", null, cv);
        db.close();
    }

    public float Sum(String product) {
        Float sum = 0f;
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT SUM(_price) as sum FROM drinks;", null);
        if (c.moveToNext()) {
            sum = c.getFloat(0);
        }

        return sum;
    }


    public void Remove(long id) {
        String string = String.valueOf(id);
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM drinks WHERE _id = '" + string + "'");
        db.close();
    }

    public List<Drink> Period(String fromDate, String toDate) throws ParseException {
        List<Drink> drinkList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM drinks WHERE _time BETWEEN '" + fromDate + "' and '" + toDate + "';", null);
        Fragment2.reportSum = 0;
        if (c != null) {

            c.moveToFirst();

            //Position after the last row means the end of the results
            while (!c.isAfterLast()) {

                //create new details object
                Drink drink = new Drink();

                //Here use static declared on top of the class..don't use "" for the table column
                drink.setID(c.getColumnIndex("_id"));
                drink.setProductName(c.getString(c.getColumnIndex("_productname")));
                drink.setPrice(c.getFloat(c.getColumnIndex("_price")));
                drink.setStars(c.getInt(c.getColumnIndex("_stars")));

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
                Date date = sdf.parse(c.getString(c.getColumnIndex("_time")));
//                drink.setTime(c.getString(c.getColumnIndex("_time")));
//                drink.setTime(sdf.format(date));
                SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm, dd-MM-yyyy");
                drink.setTime(sdf1.format((date)));
//                drink.setAddress1(c.getString(c.getColumnIndex(COLUMN_ADDRESS1)));
//                drink.setAddress2(c.getString(c.getColumnIndex(COLUMN_ADDRESS2)));
                Fragment2.reportSum = Fragment2.reportSum + drink.getPrice();
                Fragment2.tvReportSum.setPadding(3, 2, 3, 2);
                Fragment2.tvReportSum.setText(Fragment2.sum + " " + String.valueOf(Fragment2.reportSum));

                drinkList.add(drink);


                c.moveToNext();
            }

            c.close();
        }
        assert c != null;
        c.close();
        db.close();

        //return our list of persons
        return drinkList;


    }


    public float Proof(String fromDate, String toDate) {
        float dbProof = 0f;
        long timeDiff = 0;


        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        Date now = new Date();
        long timeNow = now.getTime();
        float sublim = 0;

//        Date diff =  new Date(now.getTime() )/*- date.getTime())*/;
//        try {
//            Date diff =  sdf.parse(new Date(now.getTime()).toString());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }


        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM drinks WHERE _time BETWEEN '" + fromDate + "' and '" + toDate + "';", null);
        if (c != null) {

            c.moveToFirst();

            //Position after the last row means the end of the results
            while (!c.isAfterLast()) {

//                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
                try {
                    Date dateDrink = sdf.parse(c.getString(c.getColumnIndex("_time")));
                    timeDiff = timeNow - (dateDrink.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }


//                c.getFloat(c.getColumnIndex("_proof"));
//Почасовая элиминация
                float curProof = c.getFloat(c.getColumnIndex("_proof"));
                if (curProof > 0) {
//                    dbProof = dbProof + curProof;
                    sublim = ((float) ((timeDiff / 1000 / 60 / 60) * 0.15));

                    Float r = 0f;
                    if (Main.sex == 0) {
                        r = 0.7f;
                    } else if (Main.sex == 1) {
                        r = 0.6f;
                    }
                    Float promille = (curProof / (Main.weight * r)) - (sublim);
                    if (promille > 0f) {
                        dbProof = dbProof + Main.round((promille / 100) * 20, 2);
                    }


                }

                c.moveToNext();
            }

            c.close();
        }
        assert c != null;
        c.close();
        db.close();


        return dbProof;
    }

}
