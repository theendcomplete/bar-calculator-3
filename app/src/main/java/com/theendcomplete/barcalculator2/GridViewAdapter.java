package com.theendcomplete.barcalculator2;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridViewAdapter extends ArrayAdapter {

    private ArrayList<Drink> listDrink = new ArrayList<>();
    private Activity activity;

    public GridViewAdapter(Activity activity, ArrayList<Drink> listDrink) {
        super(activity, R.layout.grid_item, listDrink);
        this.listDrink = listDrink;
        this.activity = activity;
    }

    public GridViewAdapter(Context context, int resource, ArrayList<Drink> listDrink) {
        super(context, resource);
        this.listDrink = listDrink;
    }

    @Override
    public Drink getItem(int position) {
        return listDrink.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder view;
        LayoutInflater inflator = activity.getLayoutInflater();

        if (convertView == null) {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.grid_item, null);

            view.txtViewTitle = (TextView) convertView.findViewById(R.id.tvName);

            view.imgViewFlag = (ImageView) convertView.findViewById(R.id.imageView1);

            view.txtViewCount = (TextView) convertView.findViewById(R.id.tvCount);

            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }

        view.txtViewTitle.setText(listDrink.get(position).getProductName());
        switch (listDrink.get(position).getType()) {
            case (0):
                view.imgViewFlag.setImageResource(R.drawable.beer1);
                break;
            case (1):
                view.imgViewFlag.setImageResource(R.drawable.beer2);
                break;
            case (2):
                view.imgViewFlag.setImageResource(R.drawable.shot1);
                break;
            case (3):
                view.imgViewFlag.setImageResource(R.drawable.coctail1);
                break;
            case (4):
                view.imgViewFlag.setImageResource(R.drawable.tea);
                break;
            case (5):
                view.imgViewFlag.setImageResource(R.drawable.coffee1);
                break;
            case (6):
                view.imgViewFlag.setImageResource(R.drawable.barbeque1);
                break;
            case (7):
                view.imgViewFlag.setImageResource(R.drawable.rolls);
                break;
            default:
                view.imgViewFlag.setImageResource(R.drawable.beer1);
                break;

        }

//        MyDBHelper dbHelper = new MyDBHelper(getContext());
        float count = listDrink.get(position).getCount();
        listDrink.get(position).setCount(count);
        view.txtViewCount.setText(String.valueOf(Math.round(count)));

        return convertView;
    }


    public void UpdateGrid() {
        notifyDataSetInvalidated();

    }

    public static class ViewHolder {
        //        notifyDataSetChanged();
        public ImageView imgViewFlag;
        public TextView txtViewTitle;
        public TextView txtViewCount;
    }

}