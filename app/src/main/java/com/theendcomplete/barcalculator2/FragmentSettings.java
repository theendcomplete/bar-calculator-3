package com.theendcomplete.barcalculator2;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentSettings extends DialogFragment {

    // stores the image database icons
    private static final Integer[] imageIconDatabase = {R.drawable.beer1,
            R.drawable.beer2, R.drawable.shot1, R.drawable.coctail1,
            R.drawable.tea, R.drawable.coffee1, R.drawable.barbeque1, R.drawable.rolls};
    private static Drink putDrink;
    private static int putBackInPosition;
    private EditText etPrice;
    private EditText etComment;
    private EditText tvName;
    private RadioButton rbVolume1;
    private RadioButton rbVolume2;
    private RadioButton rbVolume3;
//    private RatingBar ratingBar;


    public FragmentSettings() {

    }

    static FragmentSettings newInstance(Drink drink, int position) {
        FragmentSettings f = new FragmentSettings();
        putDrink = drink;
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("putDrink", drink);
        args.putInt("Position", position);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        putDrink = getArguments().getParcelable("putDrink");
        putBackInPosition = getArguments().getInt("Position");
        setStyle(STYLE_NORMAL, 0);
        setCancelable(false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View v = inflater.inflate(R.layout.fragment_settings, null);
        final LinearLayout llVolume = (LinearLayout) v.findViewById(R.id.llVolume);
        llVolume.setVisibility(View.INVISIBLE);
        final RadioGroup rgVolume = (RadioGroup) v.findViewById(R.id.rgVolume);
        rbVolume1 = (RadioButton) v.findViewById(R.id.rbVolume1);
        rbVolume2 = (RadioButton) v.findViewById(R.id.rbVolume2);
        rbVolume3 = (RadioButton) v.findViewById(R.id.rbVolume3);

        int intVolume = (Math.round(putDrink.getVolume()));
        rgVolume.clearCheck();
        switch (intVolume) {
            case (1):
                rbVolume1.setChecked(true);
                break;
            case (2):
                rbVolume2.setChecked(true);
                break;
            case (3):
                rbVolume3.setChecked(true);
                break;
        }


        final String[] imageNameDatabase = getActivity().getResources().getStringArray(R.array.types);
        final Spinner spinner = (Spinner) v.findViewById(R.id.spinner);
        SpinnerAdapter adapter = new SpinnerAdapter(v.getContext(), imageNameDatabase, imageIconDatabase);
        spinner.setAdapter(adapter);
        spinner.setSelection(putDrink.getType());
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                putDrink.setProof(0);
                switch (position) {
                    case 0:
                        llVolume.setVisibility(View.VISIBLE);
                        rbVolume1.setText(getString(R.string.string_033));
                        rbVolume2.setText(getString(R.string.string_05));
                        rbVolume3.setText(getString(R.string.string_1));


                        putDrink.setProof(55);
                        break;
                    case 1:
                        llVolume.setVisibility(View.VISIBLE);
                        rbVolume1.setText(getString(R.string.string_033));
                        rbVolume2.setText(getString(R.string.string_05));
                        rbVolume3.setText(getString(R.string.string_1));

                        putDrink.setProof(55);
                        break;
                    case 2:
                        llVolume.setVisibility(View.VISIBLE);
                        rbVolume1.setText(getString(R.string.string_50));
                        rbVolume2.setText(getString(R.string.string_75));
                        rbVolume3.setText(getString(R.string.string_100));
                        putDrink.setProof(400);
                        break;
                    case 3:
                        llVolume.setVisibility(View.VISIBLE);
                        rbVolume1.setText(getString(R.string.string_150));
                        rbVolume2.setText(getString(R.string.string_250));
                        rbVolume3.setText(getString(R.string.string_05));
                        putDrink.setProof(90);
                        break;
                    default:
                        llVolume.setVisibility(View.GONE);

                        break;
                }

            }

            /**
             * Callback method to be invoked when the selection disappears from this
             * view. The selection can disappear for instance when touch is activated
             * or when the adapter becomes empty.
             *
             * @param parent The AdapterView that now contains no selected item.
             */
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                llVolume.setVisibility(View.GONE);
            }

        });


        tvName = (EditText) v.findViewById(R.id.tvName);
        tvName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getActivity().getSystemService(v.getContext().INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(tvName.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return false;
            }
        });
//        tvName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                tvName.setText(getString(R.string.string_blanc));
//            }
//        });
        if (putDrink.getProductName().equals(getString(R.string.newItem))) {
            tvName.setHint(putDrink.getProductName());
            tvName.setText(R.string.string_blanc);
        } else {
            tvName.setText(putDrink.getProductName());
        }

        etPrice = (EditText) v.findViewById(R.id.etPrice);
        etPrice.setHint(getString(R.string.string_price));
        etPrice.setText(getString(R.string.string_blanc));
        if ((Math.abs(putDrink.getPrice() - 0f)) == 0) {
            etPrice.setHint(getString(R.string.string_price));
//            etPrice.setText(getString(R.string.string_blanc));

        } else {
            etPrice.setText(Float.toString(putDrink.getPrice()));

        }


        etPrice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etPrice.setText(getString(R.string.string_blanc));
            }
        });


        //Кнопки
        tvName.setHint(getString(R.string.newItem)); //чтобы не было NullPointerException


        Button btnOk = (Button) v.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (((tvName.getHint().toString().equals(getString(R.string.newItem)) && (tvName.getText().toString().equals(getString(R.string.string_blanc))))) || (tvName.getText().toString().equals(getString(R.string.string_blanc)))) {
                    Toast.makeText(v.getContext(), R.string.enter_name, Toast.LENGTH_LONG).show();

                } else {

                    putDrink.setProductName(tvName.getText().toString());
                    if (etPrice.getText().toString().equals(getString(R.string.string_blanc))) {
                        etPrice.setText(String.valueOf(0));
                    }
                    putDrink.setPrice(Float.valueOf(etPrice.getText().toString()));


                    if (rbVolume1.isChecked() == true) {
                        putDrink.setVolume(1);
                    }
                    ;
                    if (rbVolume2.isChecked() == true) {
                        putDrink.setVolume(2);
                    }
                    ;
                    if (rbVolume3.isChecked() == true) {
                        putDrink.setVolume(3);
                    }
                    ;


                    putDrink.setType((spinner.getSelectedItemPosition()));


                    //
                    switch (putDrink.getType()) {
                        case (0):
                            if (rbVolume1.isChecked()) {
                                putDrink.setProof(18);
                            } else if (rbVolume2.isChecked()) {
                                putDrink.setProof(28);
                            } else if (rbVolume3.isChecked()) {
                                putDrink.setProof(55);
                            } else
                                putDrink.setProof(0);
                            break;
                        case (1):
                            if (rbVolume1.isChecked()) {
                                putDrink.setProof(18);
                            } else if (rbVolume2.isChecked()) {
                                putDrink.setProof(28);
                            } else if (rbVolume3.isChecked()) {
                                putDrink.setProof(55);
                            } else
                                putDrink.setProof(0);
                            break;
                        case (2):
                            if (rbVolume1.isChecked()) {
                                putDrink.setProof(20);
                            } else if (rbVolume2.isChecked()) {
                                putDrink.setProof(30);
                            } else if (rbVolume3.isChecked()) {
                                putDrink.setProof(40);
                            } else
                                putDrink.setProof(0);
                            break;
                        case (3):
                            if (rbVolume1.isChecked()) {
                                putDrink.setProof(14);
                            } else if (rbVolume2.isChecked()) {
                                putDrink.setProof(23);
                            } else if (rbVolume3.isChecked()) {
                                putDrink.setProof(45);
                            } else
                                putDrink.setProof(0);
                            break;
                        case (4):
                            putDrink.setProof(0f);
                            break;
                        case (5):
                            putDrink.setProof(0f);
                            break;
                        default:
                            putDrink.setProof(0f);
                            break;
                    }


                    //переделать рейтинг во флоат?
//                    putDrink.setStars(Math.round(ratingBar.getRating()));
                    putDrink.setComment(etComment.getText().toString());
                    Main main = (Main) getActivity();
                    if (main.listDrink.size() == putBackInPosition) {
                        main.listDrink.add(putDrink);
                    } else {
                        main.listDrink.set(putBackInPosition, putDrink);
                    }
                    main.CountArray();
                    GridFragment.Update();
                    dismiss();
                }
            }
        });

        Button btnCancel = (Button) v.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        Button btnRemove = (Button) v.findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main main = (Main) getActivity();
                if (main.listDrink.size() == putBackInPosition) {
                    dismiss();
                } else {
                    main.listDrink.remove(putBackInPosition);
                    main.CountArray();
                    GridFragment.Update();
                    Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
                    FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                    fragTransaction.detach(currentFragment);
                    fragTransaction.attach(currentFragment);
                    fragTransaction.commit();
                    dismiss();

                }
            }
        });


//Кончаются Кнопки
//        ratingBar = (RatingBar) v.findViewById(R.id.ratingBar);
//        ratingBar.setProgress(putDrink.getStars());
        etComment = (EditText) v.findViewById(R.id.etComment);
        etComment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getActivity().getSystemService(v.getContext().INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(etComment.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return false;
            }
        });
        etComment.setText(putDrink.getComment());
//        etPrice = (EditText) v.findViewById(R.id.etPrice);
//        etPrice.setText(Float.toString(putDrink.getPrice()));


        return v;
    }

    public void Cancel() {
        dismiss();
    }


    @Override
    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
        window.setGravity(Gravity.TOP);
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(android.content.DialogInterface dialog, int keyCode,
                                 android.view.KeyEvent event) {

                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    //This is the filter
                    if (event.getAction() != KeyEvent.ACTION_DOWN)
                        return true;
                    else {
                        //Hide your keyboard here!!!!!!
                        return true; // pretend we've processed it
                    }
                } else
                    return false; // pass on to be processed as normal
            }
        });
    }

//    public void onDismiss(DialogInterface dialogInterface) {
////       GridFragment.Update();
//    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag);
        }
    }
}