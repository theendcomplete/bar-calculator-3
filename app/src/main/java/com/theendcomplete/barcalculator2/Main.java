package com.theendcomplete.barcalculator2;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

//import com.google.android.gms.ads.AdView;


public class Main extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks  /* DialogInterface.OnDismissListener*/ {
    static int sex = 0;

    static float weight = 0;

    final String MY_LOG = "BarCalc Log";

    public ArrayList<Drink> listDrink = new ArrayList<>();


    //    public static String[] titles ={getResources().getString(R.string.spending),getResources().getString(R.string.alcohol)};
    SharedPreferences sp;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    public static float round(float number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++)
            pow *= 10;
        float tmp = number * pow;
        return (float) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }

    public Context returnMainContext() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        titles.{getString(R.string.spending),getString(R.string.alcohol)}

        setContentView(R.layout.activity_main);

        LoadSex();


//        YourAdapter reportAdapter = new YourAdapter(this);


//        FragmentSettings settings = new FragmentSettings();

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));


    }

    public float CountArray() {
        // метод sum оставить для отчетов
        float s = 0f;
        ArrayList<Drink> list = listDrink;

        for (int i = 0; i < list.size(); i++) {
            s = s + list.get(i).getPrice() * (list.get(i).getCount());
        }
        return s;

    }

//        if (position == 0) {
// , "next").addToBackStack("prev"
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container, GridFragment.newInstance())
//                    .commit();
//
//        } else if (position == 1) {
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container, Fragment2.newInstance())
//                    .commit();
//        } else if (position == 2) {
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container, Fragment3.newInstance())
//                    .commit();
//        } else if (position == 3) {
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container, FragmentPreferences.newInstance())
//                    .commit();
//        }
//    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();


        if (position == 0) {
            fragmentManager.beginTransaction()
//                    .replace(R.id.container, GridFragment.newInstance(), "next").addToBackStack("prev")
//                    .commit();
                    .replace(R.id.container, GridFragment.newInstance())
                    .commit();

        } else if (position == 1) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, FragmentReports.newInstance())
                    .commit();
        } else if (position == 2) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, FragmentPreferences.newInstance())
                    .commit();
        }
//        else if (position == 3) {
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container, FragmentReports.newInstance())
//                    .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
//            case 4:
//                mTitle = getString(R.string.title_section4);
//                break;
        }
    }

    private void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.isShowing();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    public String[] getTitles() {


        String[] titlesMain = {getString(R.string.spending), getString(R.string.alcohol)};
//    String title1 = R.getResources().getString(R.string.title_section1);
        return titlesMain;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
//            switch (mNavigationDrawerFragment.)

            String section1 = getString(R.string.title_section1);
//            String section2 = getString(R.string.title_section2);
//            String section3 = getString(R.string.title_section3);
            if (mTitle.toString().equals(section1)) {
                getMenuInflater().inflate(R.menu.grid_fragment_menu, menu);
//                restoreActionBar();
                return true;
            }

        }
        restoreActionBar();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            SaveArray();
            return true;
        } else if (id == R.id.action_load) {
            RestoreArray();
            return true;
        } else if (id == R.id.action_clear) {
            ClearArray();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onResume() {
        super.onResume();
    }

    private void RestoreArray() {
        Gson gson = new Gson();
        String json;
        Context mContext = getApplicationContext();
        SharedPreferences mPrefs = mContext.getSharedPreferences("listDrinkDraft", Context.MODE_PRIVATE);

        json = mPrefs.getString("listDrink", "");
        Type type = new TypeToken<ArrayList<Drink>>() {
        }.getType();


        if (!json.isEmpty()) {
            listDrink = gson.fromJson(json, type);
        }

        Toast toast = Toast.makeText(getApplicationContext(), R.string.string_loaded_draft, Toast.LENGTH_SHORT);
        toast.show();


        AttachDetach();
    }

    private void SaveArray() {
        Context mContext = getApplicationContext();
        SharedPreferences mPrefs = mContext.getSharedPreferences("listDrinkDraft", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        Gson gson;
        gson = new Gson();
        ArrayList<Drink> arrayForSave = listDrink;

        for (int i = 0; i < listDrink.size(); i++) {
            arrayForSave.get(i).setCount(0);
        }

        String json = gson.toJson(arrayForSave);
        editor.putString("listDrink", json);
        editor.commit();
        Toast toast = Toast.makeText(getApplicationContext(), R.string.string_saved_draft, Toast.LENGTH_SHORT);
        toast.show();

    }

    private void ClearArray() {
        listDrink.clear();
        Toast toast = Toast.makeText(getApplicationContext(), R.string.string_cleared_array, Toast.LENGTH_SHORT);
        toast.show();
        AttachDetach();
    }

    public void AttachDetach() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.container);
        FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
        fragTransaction.detach(currentFragment);
        fragTransaction.attach(currentFragment);
        fragTransaction.commit();
    }

    public void LoadSex() {
        Context mContext = getApplicationContext();
        SharedPreferences mPrefs = mContext.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String bufferSex = mPrefs.getString("sex", "0");
        switch (Integer.parseInt(bufferSex)) {
            case (0):
                sex = 0;
                break;
            case (1):
                sex = 1;
                break;
            default:
                sex = 0;
                break;

        }

        Main.weight = Float.parseFloat(mPrefs.getString("weight", "75"));

    }
}

