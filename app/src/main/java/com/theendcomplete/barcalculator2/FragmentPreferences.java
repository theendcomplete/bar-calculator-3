package com.theendcomplete.barcalculator2;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by theendcomplete on 27.02.2015.
 */
public class FragmentPreferences extends Fragment {

    Button btnDrop;
    private EditText etWeight;
    private RadioButton rbMale;
    private RadioButton rbFemale;


    public FragmentPreferences() {
    }

    public static FragmentPreferences newInstance() {
        FragmentPreferences fragment = new FragmentPreferences();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_preferences, container, false);
        rbMale = (RadioButton) rootView.findViewById(R.id.rbMale);

        rbFemale = (RadioButton) rootView.findViewById(R.id.rbFemale);

        Context mContext = getActivity().getApplicationContext();
        SharedPreferences mPrefs = mContext.getSharedPreferences("preferences", Context.MODE_PRIVATE);

//        String savedSex = mPrefs.getString("sex", "0");
//        savedSex = Main.sex;
        switch (Main.sex) {
            case (0):
                rbMale.setChecked(true);
                rbFemale.setChecked(false);
                break;
            case (1):
                rbMale.setChecked(false);
                rbFemale.setChecked(true);
                break;
            default:
                rbMale.setChecked(true);
                rbFemale.setChecked(false);
                break;
        }


        etWeight = (EditText) rootView.findViewById(R.id.etWeight);
        etWeight.setText(String.valueOf(Main.weight));
        etWeight.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getActivity().getSystemService(v.getContext().INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(etWeight.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                return false;
            }
        });


        btnDrop = (Button) rootView.findViewById(R.id.btnDrop);
        btnDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDBHelper dbHandler = new MyDBHelper(getActivity().getApplicationContext());
                dbHandler.DeleteTable();
                dbHandler.close();
            }
        });
        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((Main) activity).onSectionAttached(3);
    }

    /**
     * Called when the Fragment is no longer resumed.  This is generally
     * tied to {@link android.app.Activity#onPause() Activity.onPause} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onPause() {
        super.onPause();
        SavePreferences();


    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void SavePreferences() {

        if (rbMale.isChecked()) {
            Main.sex = 0;
        } else if (rbFemale.isChecked()) {
            Main.sex = 1;
        }
        Main.weight = Float.valueOf(etWeight.getText().toString());
        Context mContext = getActivity().getApplicationContext();
        SharedPreferences mPrefs = mContext.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("sex", String.valueOf(Main.sex));
        editor.putString("weight", String.valueOf(Main.weight));
        editor.commit();
//        Toast toast = Toast.makeText(getActivity().getApplicationContext(), R.string.string_saved_draft, Toast.LENGTH_SHORT);
//        toast.show();

    }
}

