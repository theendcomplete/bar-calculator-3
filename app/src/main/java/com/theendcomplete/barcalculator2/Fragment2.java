package com.theendcomplete.barcalculator2;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class Fragment2 extends Fragment {

    public static float reportSum = 0f;
    public static TextView tvReportSum;
    public static String sum = "";
    private static String startDate;
    private static String endDate;
    private Button btnStart;
    private Button btnEnd;
    private List<Drink> drinkList = new ArrayList<>();
    private ListView mListView;
    private DetailsAdapter adapter;

    public Fragment2() {
    }

    public static Fragment2 newInstance() {

        return new Fragment2();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sum = getString(R.string.sum);
        tvReportSum = (TextView) getActivity().findViewById(R.id.tvReportSum);
        setHasOptionsMenu(false);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment2, container, false);

        tvReportSum = (TextView) rootView.findViewById(R.id.tvReportSum);

        Calendar c = Calendar.getInstance();
        c.getTime();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

        startDate = sdf.format(c.getTime());


        c.getTime();
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);

//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

        endDate = sdf.format(c.getTime());


        btnStart = (Button) rootView.findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, month);
                        myCalendar.set(Calendar.DAY_OF_MONTH, day);
                        myCalendar.set(Calendar.HOUR_OF_DAY, 0);
                        myCalendar.set(Calendar.MINUTE, 0);
                        myCalendar.set(Calendar.SECOND, 0);
                        myCalendar.set(Calendar.MILLISECOND, 0);

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS", Locale.US);
                        btnStart.setText(day + "." + (month + 1) + "." + year);

                        startDate = sdf.format(myCalendar.getTime());
                    }
                };

                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });
        btnEnd = (Button) rootView.findViewById(R.id.btnEnd);
        btnEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, month);
                        myCalendar.set(Calendar.DAY_OF_MONTH, day);
                        myCalendar.set(Calendar.HOUR_OF_DAY, 23);
                        myCalendar.set(Calendar.MINUTE, 59);
                        myCalendar.set(Calendar.SECOND, 59);
                        myCalendar.set(Calendar.MILLISECOND, 0);

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS", Locale.US);

                        btnEnd.setText(day + "." + (month + 1) + "." + year);
                        endDate = sdf.format(myCalendar.getTime());
                    }
                };
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });

        Button btnRun = (Button) rootView.findViewById(R.id.btnRun);
        btnRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RunQuery();

            }
        });


        //get an instance of our listview
        mListView = (ListView) rootView.findViewById(R.id.listView);

        //Initiate our adapter
        adapter = new DetailsAdapter(getActivity().getApplicationContext(), drinkList);

        //set adapter to the listview
        mListView.setAdapter(adapter);

        setHasOptionsMenu(true);
        return rootView;
    }


    private void RunQuery() {

        if (startDate.compareTo(endDate) > 0) {

            Toast toast = Toast.makeText(getActivity().getApplicationContext(), R.string.wrongdate, Toast.LENGTH_SHORT);
            toast.show();
        } else {
            MyDBHelper dbHandler = new MyDBHelper(getActivity().getApplicationContext());
            try {
                drinkList = dbHandler.Period(startDate, endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //Initiate our adapter
            adapter = new DetailsAdapter(getActivity().getApplicationContext(), drinkList);

            //set adapter to the listview
            mListView.setAdapter(adapter);
        }


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((Main) activity).onSectionAttached(2);
    }

}