package com.theendcomplete.barcalculator2;

/**
 * Created by theendcomplete on 29.04.2015.
 */
final class ProofResult {
    private float proof;
    private float sublim;

    public ProofResult(float first, float second) {
        this.proof = first;
        this.sublim = second;
    }

    public float getProof() {
        return proof;
    }


    public void setProof(float a) {
        this.proof = a;
    }

    public float getSublim() {
        return sublim;
    }

    public void setSublim(float a) {
        this.sublim = a;
    }

}