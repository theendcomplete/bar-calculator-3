package com.theendcomplete.barcalculator2;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by nicko_000 on 21.01.2015.
 */
public class Fragment_Alcohol extends Fragment {
    private static String startDate;
    private static String endDate;
    TextView tvProof;

    public Fragment_Alcohol() {
    }

    public static Fragment_Alcohol newInstance() {
        Fragment_Alcohol fragment = new Fragment_Alcohol();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment3, container, false);
        tvProof = (TextView) v.findViewById(R.id.tvProof);
//        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, -2);
//        c.getTime();
//        c.set(Calendar.HOUR_OF_DAY, 0);
//        c.set(Calendar.MINUTE, 0);
//        c.set(Calendar.SECOND, 0);
//        c.set(Calendar.MILLISECOND, 0);
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
//
//        startDate = sdf.format(c.getTime());
//        Calendar c2 = Calendar.getInstance();
//
//        c2.getTime();
//        c2.set(Calendar.HOUR_OF_DAY, 23);
//        c2.set(Calendar.MINUTE, 59);
//        c2.set(Calendar.SECOND, 59);
//        c2.set(Calendar.MILLISECOND, 999);
//        endDate = sdf.format(c2.getTime());
//
//
//        MyDBHelper dbHandler = new MyDBHelper(getActivity().getApplicationContext());
//        Float proof = dbHandler.Proof(startDate, endDate);
//        Float r = 0f;
//        if (Main.sex == 0) {
//            r = 0.7f;
//        } else if (Main.sex == 1) {
//            r = 0.6f;
//        }
//        Float promille = proof / (Main.weight * r);
//        promille = Main.round((promille / 100) * 30, 2);
////        promille = promille - (promille/100) * 30;
//        tvProof.setText("your current blood alcohol level is ~" + String.valueOf(promille) + " ‰");
        Promille();
        return v;
    }

    public void Promille() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -2);
        c.getTime();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

        startDate = sdf.format(c.getTime());
        Calendar c2 = Calendar.getInstance();

        c2.getTime();
        c2.set(Calendar.HOUR_OF_DAY, 23);
        c2.set(Calendar.MINUTE, 59);
        c2.set(Calendar.SECOND, 59);
        c2.set(Calendar.MILLISECOND, 999);
        endDate = sdf.format(c2.getTime());


        MyDBHelper dbHandler = new MyDBHelper(getActivity().getApplicationContext());
        float proof = dbHandler.Proof(startDate, endDate);
//        Float r = 0f;
//        if (Main.sex == 0) {
//            r = 0.7f;
//        } else if (Main.sex == 1) {
//            r = 0.6f;
//        }
//        Float promille = (proof.getProof() / (Main.weight * r)) - (proof.getSublim() * 100);
//        if (promille > 0f) {
//        promille = Main.round((promille / 100) * 30, 2);
//        } else {
//            promille = 0f;
//        }

//        promille = promille - (promille/100) * 30;
        tvProof.setText("your current blood alcohol level is ~" + String.valueOf(proof) + " ‰");

    }


    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link android.app.Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onResume() {
        super.onResume();
        Promille();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((Main) activity).onSectionAttached(4);
    }
}