package com.theendcomplete.barcalculator2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class YourAdapter extends FragmentStatePagerAdapter {
    private String[] titles = {"$", "‰"};

    public YourAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0: {
                return Fragment2.newInstance();
            }
            case 1: {
                return Fragment_Alcohol.newInstance();
            }
//            case 2: {
//                return Fragment3.newInstance();
//            }
        }
        return null;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}