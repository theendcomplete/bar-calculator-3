package com.theendcomplete.barcalculator2;

/**
 * Created by theendcomplete on 24.02.2015.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


class SpinnerAdapter extends ArrayAdapter<String> {

    private final Context ctx;
    private final String[] contentArray;
    private final Integer[] imageArray;

    public SpinnerAdapter(Context context, String[] objects,
                          Integer[] imageArray) {
        super(context, R.layout.spinner_layout, R.id.imageNameSpinner, objects);
        this.ctx = context;
        this.contentArray = objects;
//        this.contentArray = R.array.types_array.g;
        this.imageArray = imageArray;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.spinner_layout, parent, false);

//        TextView textView = (TextView) row.findViewById(R.id.imageNameSpinner);
        TextView textView = (TextView) row.findViewById(R.id.imageNameSpinner);
        textView.setText(contentArray[position]);

        ImageView imageView = (ImageView) row.findViewById(R.id.imageIconSpinner);
        imageView.setImageResource(imageArray[position]);

        return row;
    }
}