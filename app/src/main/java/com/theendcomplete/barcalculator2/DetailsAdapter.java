package com.theendcomplete.barcalculator2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by theendcomplete on 16.03.2015.
 */
public class DetailsAdapter extends ArrayAdapter<Drink> {

    private final Context context;

    public DetailsAdapter(Context context, List<Drink> objects) {
        super(context, R.layout.row_layout, objects);
        this.context = context;
    }


    @Override
    public View getView(final int position, View row, final ViewGroup parent) {

        Holder holder;

        if (row == null) {

            holder = new Holder();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            row = inflater.inflate(R.layout.row_layout, null);

            holder.tvRowProduct = (TextView) row.findViewById(R.id.tvRowProduct);
            holder.tvRowPrice = (TextView) row.findViewById(R.id.tvRowPrice);
//            holder.tvRowStars = (TextView) row.findViewById(R.id.tvRowStars);
            holder.tvRowDate = (TextView) row.findViewById(R.id.tvRowDate);
            holder.btnRemoveRow = (Button) row.findViewById(R.id.btnRemoveRow);
            holder.btnRemoveRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyDBHelper dbHandler = new MyDBHelper(getContext());

                    dbHandler.Remove(getItemId(position));

                }
            });

            row.setTag(holder);

        } else {

            holder = (Holder) row.getTag();
        }

        Drink drink = getItem(position);

        holder.tvRowProduct.setText(drink.getProductName());
        holder.tvRowPrice.setText(String.valueOf(drink.getPrice()));
//        holder.tvRowStars.setText(String.valueOf(drink.getStars()));
        holder.tvRowDate.setText(drink.getTime());
        return row;
    }

    //A holder will be resposable to hold our components to improve listview performance
//We replicate the components we have in the row_details.xml
    private class Holder {

        TextView tvRowProduct;
        TextView tvRowPrice;
        TextView tvRowStars;
        TextView tvRowDate;
        Button btnRemoveRow;


    }


}